﻿/*
 * Kobold Cleaning!
 * 7DRL Entry - https://7drl.com/
 *
 * Author: Jackalsoft Games
 * Date:   03/05/2022 to 03/12/2022
 * 
 * This code is released under the GPL v3.0 license.
 * All other rights (c) their respective parties.
 */

namespace Kobold
{
    #region using

    using System;
    using System.Collections.Generic;

    using SFML;
    using SFML.Audio;
    using SFML.Graphics;
    using SFML.System;
    using SFML.Window;

    using Squish;
    using Squish.Collections;
    using Squish.Extensions;
    using Squish.Graphics;
    using Squish.Utilities;

    #endregion

    public class DirtController :
        Controller
    {
        public DirtController(World world) :
            base(world)
        {
            int x = world.Bounds.W;
            int y = world.Bounds.H;

            Wet = new Grid<int>(x, y);
            Mud = new Grid<int>(x, y);
            Dust = new Grid<int>(x, y);
            Dirt = new Grid<int>(x, y);
        }

        // Walking causes dirt
        // Broom removes dirt

        // Time causes dust
        // Cloth removes dust

        // Mop causes wet
        // Cloth removes wet

        // Dirt and wet cause mud
        // Time removes mud and adds dirt

        public Grid<int> Wet;
        public Grid<int> Mud;
        public Grid<int> Dust;
        public Grid<int> Dirt;
    }
}