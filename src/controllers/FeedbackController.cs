﻿/*
 * Kobold Cleaning!
 * 7DRL Entry - https://7drl.com/
 *
 * Author: Jackalsoft Games
 * Date:   03/05/2022 to 03/12/2022
 * 
 * This code is released under the GPL v3.0 license.
 * All other rights (c) their respective parties.
 */

namespace Kobold
{
    #region using

    using System;
    using System.Collections.Generic;

    using SFML;
    using SFML.Audio;
    using SFML.Graphics;
    using SFML.System;
    using SFML.Window;

    using Squish;
    using Squish.Collections;
    using Squish.Extensions;
    using Squish.Graphics;
    using Squish.Utilities;

    #endregion

    // Handles audio, text, particles, etc
    public class FeedbackController :
        Controller
    {
        public FeedbackController(World world) :
            base(world)
        {
            Messages = new List<string>();
            Color = new List<c32>();
        }

        public List<string> Messages;
        public List<c32> Color;

        public void AddMessage(string text, c32 color)
        {
            Messages.Add(text);
            Color.Add(color);
        }

        public void AddMessage(string text)
        {
            Messages.Add(text);
            Color.Add(c32.White);
        }

        public void ClearMessages()
        {
            Messages.Clear();
        }
    }
}