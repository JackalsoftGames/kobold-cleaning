﻿/*
 * Kobold Cleaning!
 * 7DRL Entry - https://7drl.com/
 *
 * Author: Jackalsoft Games
 * Date:   03/05/2022 to 03/12/2022
 * 
 * This code is released under the GPL v3.0 license.
 * All other rights (c) their respective parties.
 */

namespace Kobold
{
    #region using

    using System;
    using System.Collections.Generic;

    using SFML;
    using SFML.Audio;
    using SFML.Graphics;
    using SFML.System;
    using SFML.Window;

    using Squish;
    using Squish.Collections;
    using Squish.Extensions;
    using Squish.Graphics;
    using Squish.Utilities;

    #endregion

    public class CombatController :
        Controller
    {
        public CombatController(World world) :
            base(world)
        {
            Roll = new Random();
            Stats = new Dictionary<Entity, CombatStats>();
        }

        public Random Roll;
        public Dictionary<Entity, CombatStats> Stats;

        public void SetSeed(int? seed = null)
        {
            if (seed == null)
                seed = System.Environment.TickCount;

            Roll = new Random(seed.Value);
        }

        public void Attack(CombatStats a, CombatStats b)
        {
            string str = "";
            str += String.Format("{0} {2} {1}", a.Entity.Name, b.Entity.Name, a.Entity.textAttack);

            if (!RollToHit(a, b))
            {
                str += String.Format(" but {0} ignores it!", b.Entity.Name);
                World.Feedback.AddMessage(str);
                return;
            }

            int damage = GetMod(a.Str) + Roll.Next(a.DamageMin, a.DamageMax);
            damage -= b.Armor;

            if (damage < 1)
                damage = 1;

            b.Life -= damage;
            str += String.Format("!");

            if (b.Life <= 0)
                str += String.Format(" {0} runs away!", b.Entity.Name);

            c32 c = c32.White;
            if (a.Entity == World.Player)
                c = a.Entity.Gloss.DrawArgs.Color;

            World.Feedback.AddMessage(str, c);
        }

        public bool RollToHit(CombatStats a, CombatStats b)
        {
            int roll = Roll.Next(1, 21);

            if (roll == 1) return false;
            if (roll == 20) return true;

            int hit = a.ToHit + GetMod(a.Dex);
            int ac = b.Dodge + GetMod(b.Dex);

            return (hit + roll >= ac);
        }

        public int GetMod(int value)
        {
            return (value - 10) / 2;
        }
    }

    public class CombatStats
    {
        public CombatStats(Entity entity)
        {
            Entity = entity;
        }

        public Entity Entity;

        public int Life;
        public int LifeMax;

        public int DamageMin;
        public int DamageMax;

        public int ToHit;
        public int Dodge;
        public int Armor;

        public int Str;
        public int Dex;
        public int Con;
        public int Int;
        public int Wis;
        public int Cha;
    }
}