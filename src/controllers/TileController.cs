﻿/*
 * Kobold Cleaning!
 * 7DRL Entry - https://7drl.com/
 *
 * Author: Jackalsoft Games
 * Date:   03/05/2022 to 03/12/2022
 * 
 * This code is released under the GPL v3.0 license.
 * All other rights (c) their respective parties.
 */

namespace Kobold
{
    #region using

    using System;
    using System.Collections.Generic;

    using SFML;
    using SFML.Audio;
    using SFML.Graphics;
    using SFML.System;
    using SFML.Window;

    using Squish;
    using Squish.Collections;
    using Squish.Extensions;
    using Squish.Graphics;
    using Squish.Utilities;

    #endregion

    public class TileController :
        Controller
    {
        public TileController(World world) :
            base(world)
        {}

        public void SetMerge(v2i pos)
        {
            if (!Exists(pos))
                return;

            bool solid = World.IsSolid[pos];
            Direction result = 0;

            if (solid == World.IsSolid[pos.Step(Direction.N)]) result |= Direction.N;
            if (solid == World.IsSolid[pos.Step(Direction.E)]) result |= Direction.E;
            if (solid == World.IsSolid[pos.Step(Direction.S)]) result |= Direction.S;
            if (solid == World.IsSolid[pos.Step(Direction.W)]) result |= Direction.W;

            if (solid == World.IsSolid[pos.Step(Direction.NE)]) result |= Direction.NE;
            if (solid == World.IsSolid[pos.Step(Direction.SE)]) result |= Direction.SE;
            if (solid == World.IsSolid[pos.Step(Direction.SW)]) result |= Direction.SW;
            if (solid == World.IsSolid[pos.Step(Direction.NW)]) result |= Direction.NW;

            World.Merge[pos] = result;
        }

        public void SetTile(v2i pos, bool solid, bool solidLight, bool indoor)
        {
            World.IsSolid[pos] = solid;
            World.IsSolidLight[pos] = solidLight;
            World.IsIndoor[pos] = indoor;
        }

        public void SetVisible(v2i pos, bool visible, bool revealed)
        {
            World.IsVisible[pos] = visible;
            World.IsRevealed[pos] = revealed;
        }

        public void SetGloss(v2i pos, int icon)
        {
            World.Icon[pos] = icon;
        }

        public void Clear(v2i pos)
        {
            SetTile(pos, false, false, false);
            SetVisible(pos, false, false);
        }

        public void Clear()
        {
            World.IsSolid.Fill(false);
            World.IsSolidLight.Fill(false);
            World.IsIndoor.Fill(false);

            World.IsVisible.Fill(false);
            World.IsRevealed.Fill(false);

            World.Icon.Fill(0);
            World.Merge.Fill(0);
        }

        public void RevealMap(bool value)
        {
            World.IsVisible.Fill(value);
            World.IsRevealed.Fill(value);
        }

        public bool Exists(v2i pos)
        {
            return World.Bounds.Contains(pos);
        }

        public bool Exists(int x, int y)
        {
            return Exists(new v2i(x, y));
        }
    }
}