﻿/*
 * Kobold Cleaning!
 * 7DRL Entry - https://7drl.com/
 *
 * Author: Jackalsoft Games
 * Date:   03/05/2022 to 03/12/2022
 * 
 * This code is released under the GPL v3.0 license.
 * All other rights (c) their respective parties.
 */

namespace Kobold
{
    #region using

    using System;
    using System.Collections.Generic;

    using SFML;
    using SFML.Audio;
    using SFML.Graphics;
    using SFML.System;
    using SFML.Window;

    using Squish;
    using Squish.Collections;
    using Squish.Extensions;
    using Squish.Graphics;
    using Squish.Utilities;

    #endregion

    public class EntityController :
        Controller
    {
        public EntityController(World world) :
            base(world)
        {
            m_ObjectCount = 0;
        }

        private int m_ObjectCount;

        public Entity ActorAt(v2i pos)
        {
            foreach (var ITEM in World.Actors)
            {
                if (pos == ITEM.Pos)
                    return ITEM;
            }

            return null;
        }

        public Entity PropAt(v2i pos)
        {
            foreach (var ITEM in World.Props)
            {
                if (pos == ITEM.Pos)
                    return ITEM;
            }

            return null;
        }

        public Entity CreateActor(string name, v2i pos)
        {
            Entity result = new Entity();
            World.Actors.Add(result);

            result.ID = m_ObjectCount++;
            result.Name = name;
            result.Pos = pos;
            return result;
        }

        public Entity CreateProp(string name, v2i pos)
        {
            Entity result = new Entity();
            World.Props.Add(result);

            result.ID = m_ObjectCount++;
            result.Name = name;
            result.Pos = pos;

            return result;
        }

        public void Step(Entity entity, Direction direction)
        {
            switch (direction)
            {
                case Direction.N: MoveTo(entity, entity.Pos + new v2i(0, -1)); break;
                case Direction.E: MoveTo(entity, entity.Pos + new v2i(+1, 0)); break;
                case Direction.S: MoveTo(entity, entity.Pos + new v2i(0, +1)); break;
                case Direction.W: MoveTo(entity, entity.Pos + new v2i(-1, 0)); break;

                case Direction.NE: MoveTo(entity, entity.Pos + new v2i(+1, -1)); break;
                case Direction.SE: MoveTo(entity, entity.Pos + new v2i(+1, +1)); break;
                case Direction.SW: MoveTo(entity, entity.Pos + new v2i(-1, +1)); break;
                case Direction.NW: MoveTo(entity, entity.Pos + new v2i(-1, -1)); break;
            }
        }

        public bool CanMove(v2i pos)
        {
            if (!World.Bounds.Contains(pos))
                return false;

            if (World.Entity.PropAt(pos) == null)
            {
                if (World.IsSolid[pos])
                    return false;
            }
            else
            {
                World.Entity.PropAt(pos).Vars["open"] = 1;
                World.Entity.PropAt(pos).Vars["close"] = 0;
            }

            foreach (var ITEM in World.Actors)
            {
                if (pos == ITEM.Pos)
                    return false;
            }

            return true;
        }

        public void MoveTo(Entity entity, v2i pos)
        {
            if (entity == null)
                return;

            if (pos.X < entity.Pos.X) entity.Gloss.FlipH = true;
            if (pos.X > entity.Pos.X) entity.Gloss.FlipH = false;

            if (CanMove(pos))
            {
                entity.Pos = pos;
            }
            else
            {
                Entity other = ActorAt(pos);
                if (other != null && other != World.Player)
                {
                    World.Combat.Attack(World.Combat.Stats[entity], World.Combat.Stats[other]);
                    if (World.Combat.Stats[other].Life <= 0)
                    {
                        World.Actors.Remove(other);
                        //World.Combat.Stats.Remove(other);

                        // This is beyond hacky
                        if (other.Name == "Bat") GameInstance.DeadBats++;
                        if (other.Name == "Mouse") GameInstance.DeadMice++;
                    }
                }
            }
        }

        public void SetMouseVars(Entity a, c32 color)
        {
            World w = World;

            a.Gloss.Frames.Source = Frames.Create(96, 32, 16, 16, 2, 2);
            a.Gloss.DrawArgs.Color = color;
            a.Gloss.AnimSpeed = 1;

            w.Combat.Stats[a] = new CombatStats(a);
            DoCoreStats(w.Combat.Stats[a], 20, 3, 8);
            DoCombatStats(w.Combat.Stats[a], 5, 10, 2, 4, 1);

            a.Name = "Mouse";
            a.textAttack = "chitters at";
        }

        public void SetBatVars(Entity a, c32 color)
        {
            World w = World;

            a.Gloss.Frames.Source = Frames.Create(64, 32, 16, 16, 2, 2);
            a.Gloss.DrawArgs.Color = color;
            a.Gloss.AnimSpeed = 2;

            w.Combat.Stats[a] = new CombatStats(a);
            DoCoreStats(w.Combat.Stats[a], 10, 6, 12);
            DoCombatStats(w.Combat.Stats[a], 6, 12, 1, 3, 0);

            a.Name = "Bat";
            a.textAttack = "flaps against";
        }

        public void DoPlayerStats()
        {
            World w = World;

            if (!w.Combat.Stats.ContainsKey(w.Player))
                w.Combat.Stats[w.Player] = new CombatStats(w.Player);

            DoCoreStats(w.Combat.Stats[w.Player], 100, 3, 18);
            DoCombatStats(w.Combat.Stats[w.Player], 7, 1, 2, 8, 1);
        }

        public void DoCoreStats(CombatStats stats, int life, int min, int max)
        {
            Random r = World.Combat.Roll;

            stats.Life = life;
            stats.LifeMax = life;

            stats.Str = r.Next(min, max + 1);
            stats.Dex = r.Next(min, max + 1);
            stats.Con = r.Next(min, max + 1);
            stats.Int = r.Next(min, max + 1);
            stats.Wis = r.Next(min, max + 1);
            stats.Cha = r.Next(min, max + 1);
        }

        public void DoCombatStats(CombatStats stats, int attack, int dodge, int min, int max, int armor)
        {
            stats.ToHit = attack;
            stats.Dodge = dodge;
            stats.DamageMin = min;
            stats.DamageMax = max;
            stats.Armor = armor;
        }
    }
}