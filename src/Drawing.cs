﻿/*
 * Kobold Cleaning!
 * 7DRL Entry - https://7drl.com/
 *
 * Author: Jackalsoft Games
 * Date:   03/05/2022 to 03/12/2022
 * 
 * This code is released under the GPL v3.0 license.
 * All other rights (c) their respective parties.
 */

namespace Kobold
{
    #region using

    using System;
    using System.Collections.Generic;

    using SFML;
    using SFML.Audio;
    using SFML.Graphics;
    using SFML.System;
    using SFML.Window;

    using Squish;
    using Squish.Collections;
    using Squish.Extensions;
    using Squish.Graphics;
    using Squish.Utilities;

    #endregion

    public class DrawHelper :
        Controller
    {
        public DrawHelper(World world, RenderTarget target) :
            base(world)
        {
            Target = target;
            States = RenderStates.Default;

            Buffer = new QuadBuffer();
            Texture = new Texture("res/img/images.png");
            Font = new Font("res/font/old_wizard.ttf");
            Frames = new Frames(Texture, Frames.Create(0, 0, 16, 16, 256, 16));
            Camera = new Camera(0, 0, 15, 15);

            Palette = new Dictionary<string, c32>();
        }

        public RenderTarget Target;
        public RenderStates States;

        public QuadBuffer Buffer;
        public Texture Texture;
        public Font Font;
        public Frames Frames;
        public Camera Camera;

        public Dictionary<string, c32> Palette;


        public void DrawEntity(Entity entity)
        {
            if (entity == null || entity.Gloss == null)
                return;

            Quad q = entity.Gloss.GetQuad();
            Buffer.Add(q);
        }

        public void DrawBar(v2f position, v2f size, float percent, c32 c1, c32 c2)
        {
            Buffer.Add(Quad.FromSprite(position, size, null, rectf.Empty, c1, 0.00f));
            Buffer.Add(Quad.FromSprite(position, new v2f(size.X * percent, size.Y), null, rectf.Empty, c2, 0.00f));
        }

        public void DrawText(string message, c32 color, float x, float y, uint size)
        {
            Text text = new Text(message, Font, size);
            text.Color = color;
            text.Position = new v2f(x, y);

            Target.Draw(text, States);
        }

        public void DrawTiles(World world, recti bounds, float depth)
        {
            const float DEPTH_TILE = 0;
            const float DEPTH_SHADOW = 1;
            const float DEPTH_BASEBOARD = 2;
            const float DEPTH_HIDDEN = 4;
            const float DEPTH_OUTLINE = 8;
            
            rectf[] frames = Frames.Create(192, 224, 16, 16, 8, 4);

            bounds = bounds.Intersection(world.Bounds);

            float maxDist = 9 * 9;
            v2f mid = (v2f)world.Player.Pos;

            foreach (var POS in bounds)
            {
                if (!world.IsRevealed[POS])
                    continue;

                v2f p = (v2f)POS;
                Direction merge = world.Merge[POS];
                bool solid = world.IsSolid[POS];
                bool indoor = world.IsIndoor[POS];

                // Main graphics
                {
                    c32 c = c32.White;

                    // Light
                    if (indoor)
                    {
                        float percent = MathF.Clamp((p - mid).LengthSquared / maxDist, 0, 1);
                        c = c.Lerp(c32.FromPacked(0x000000), percent);
                    }

                    // Tile
                    Buffer.Add(Quad.FromSprite(p, v2f.One, Texture, Frames.Source[world.Icon[POS]], c, depth + DEPTH_TILE));

                    // Shadow
                    if (!solid)
                    {
                        if ((world.Merge[POS] & Direction.N) == 0)
                            Buffer.Add(Quad.FromSprite(p, v2f.One, Texture, frames[6], c32.Black * 0.50f, depth + DEPTH_SHADOW));
                    }

                    // Baseboard
                    else
                    {
                        if ((world.Merge[POS] & Direction.S) == 0)
                        {
                            Buffer.Add(Quad.FromSprite(p, v2f.One, Texture, frames[5], c32.White, depth + DEPTH_BASEBOARD));
                        }
                    }

                    // Outline
                    if (solid) // Can change with eg visual style ("nightvision")
                    {
                        if ((merge & Direction.N) == 0) Buffer.Add(Quad.FromSprite(p, v2f.One, Texture, frames[0], c32.White, depth + DEPTH_OUTLINE));
                        if ((merge & Direction.E) == 0) Buffer.Add(Quad.FromSprite(p, v2f.One, Texture, frames[1], c32.White, depth + DEPTH_OUTLINE));
                        if ((merge & Direction.S) == 0) Buffer.Add(Quad.FromSprite(p, v2f.One, Texture, frames[2], c32.White, depth + DEPTH_OUTLINE));
                        if ((merge & Direction.W) == 0) Buffer.Add(Quad.FromSprite(p, v2f.One, Texture, frames[3], c32.White, depth + DEPTH_OUTLINE));
                    }
                }

                // Hidden
                if (!world.IsVisible[POS])
                {
                    c32 c = c32.White.Lerp(c32.Black, 0.50f) *1.00f;

                    if (world.IsIndoor[POS])
                        c = c32.Black;

                    Buffer.Add(Quad.FromSprite(p, v2f.One, Texture, frames[7], c, depth + DEPTH_HIDDEN));
                }
            }
        }

        public void DrawBox(float x, float y, float w, float h, Texture texture, rectf source, c32 color)
        {
            Buffer.Add(Quad.FromSprite(new rectf(x, y, w, h), texture, source, color, 0.00f));
        }
    }
}