﻿/*
 * Kobold Cleaning!
 * 7DRL Entry - https://7drl.com/
 *
 * Author: Jackalsoft Games
 * Date:   03/05/2022 to 03/12/2022
 * 
 * This code is released under the GPL v3.0 license.
 * All other rights (c) their respective parties.
 */

namespace Kobold
{
    #region using

    using System;
    using System.Collections.Generic;

    using SFML;
    using SFML.Audio;
    using SFML.Graphics;
    using SFML.System;
    using SFML.Window;

    using Squish;
    using Squish.Collections;
    using Squish.Extensions;
    using Squish.Graphics;
    using Squish.Utilities;

    #endregion

    public class VarSet
    {
        public float this[string name]
        {
            get
            {
                if (Items.ContainsKey(name))
                    return Items[name];

                return 0.00f;
            }
            set
            {
                Items[name] = value;
            }
        }

        public Dictionary<string, float> Items = new Dictionary<string, float>();

        public bool Exists(string name)
        {
            return Items.ContainsKey(name);
        }

        public bool IsTrue(string name)
        {
            return
                Items.ContainsKey(name) &&
                Items[name] > 0.00f;
        }

        public bool IsFalse(string name)
        {
            return
                Items.ContainsKey(name) &&
                Items[name] <= 0.00f;
        }

        public bool IsNull(string name)
        {
            return !Items.ContainsKey(name);
        }

        public bool IsNullOrZero(string name)
        {
            return
                !Items.ContainsKey(name) ||
                Items[name] == 0.00f;
        }
    }
}