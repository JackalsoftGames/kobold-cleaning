﻿/*
 * Kobold Cleaning!
 * 7DRL Entry - https://7drl.com/
 *
 * Author: Jackalsoft Games
 * Date:   03/05/2022 to 03/12/2022
 * 
 * This code is released under the GPL v3.0 license.
 * All other rights (c) their respective parties.
 */

namespace Kobold
{
    #region using

    using System;
    using System.Collections.Generic;

    using SFML;
    using SFML.Audio;
    using SFML.Graphics;
    using SFML.System;
    using SFML.Window;

    using Squish;
    using Squish.Collections;
    using Squish.Extensions;
    using Squish.Graphics;
    using Squish.Utilities;

    #endregion

    public static class Extensions
    {
        public static v2i Step(this v2i obj, Direction value)
        {
            switch (value)
            {
                case Direction.N: return new v2i(obj.X, obj.Y - 1);
                case Direction.E: return new v2i(obj.X + 1, obj.Y);
                case Direction.S: return new v2i(obj.X, obj.Y + 1);
                case Direction.W: return new v2i(obj.X - 1, obj.Y);

                case Direction.NE: return new v2i(obj.X + 1, obj.Y - 1);
                case Direction.SE: return new v2i(obj.X + 1, obj.Y + 1);
                case Direction.SW: return new v2i(obj.X - 1, obj.Y + 1);
                case Direction.NW: return new v2i(obj.X - 1, obj.Y - 1);
            }

            return obj;
        }

        public static Direction GetEdge(this recti obj, v2i pos)
        {
            Direction result = 0;

            if (pos.X == obj.X) result |= Direction.W;
            if (pos.Y == obj.Y) result |= Direction.N;
            if (pos.X == obj.X + obj.W - 1) result |= Direction.E;
            if (pos.Y == obj.Y + obj.H - 1) result |= Direction.S;

            return result;
        }

        public static Direction GetNextDirection(this Random obj, bool diagonal = false)
        {
            if (diagonal)
                return (Direction)(0x01 << obj.Next(0, 8));
            else
                return (Direction)(0x01 << obj.Next(0, 4));
        }

        public static v2i GetNextStep(this Random obj, bool diagonal = false)
        {
            Direction d = obj.GetNextDirection(diagonal);
            return v2i.Zero.Step(d);
        }
    }
}