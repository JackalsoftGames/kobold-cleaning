﻿/*
 * Kobold Cleaning!
 * 7DRL Entry - https://7drl.com/
 *
 * Author: Jackalsoft Games
 * Date:   03/05/2022 to 03/12/2022
 * 
 * This code is released under the GPL v3.0 license.
 * All other rights (c) their respective parties.
 */

namespace Kobold
{
    #region using

    using System;
    using System.Collections.Generic;

    using SFML;
    using SFML.Audio;
    using SFML.Graphics;
    using SFML.System;
    using SFML.Window;

    using Squish;
    using Squish.Collections;
    using Squish.Extensions;
    using Squish.Graphics;
    using Squish.Utilities;

    #endregion

    public class World
    {
        public World(RenderTarget target)
        {
            Tile = new TileController(this);
            Combat = new CombatController(this);
            Entity = new EntityController(this);
            Feedback = new FeedbackController(this);
            Dirt = new DirtController(this);

            Drawing = new DrawHelper(this, target);
        }

        // Controllers
        public TileController Tile { get; private set; }
        public CombatController Combat { get; private set; }
        public EntityController Entity { get; private set; }
        public FeedbackController Feedback { get; private set; }
        public DirtController Dirt { get; private set; }

        public DrawHelper Drawing { get; private set; }

        public recti Bounds;

        // Tiles
        public Grid<bool> IsSolid;
        public Grid<bool> IsSolidLight;
        public Grid<bool> IsIndoor;

        public Grid<bool> IsVisible;
        public Grid<bool> IsRevealed;

        public Grid<int> Icon;
        public Grid<Direction> Merge;

        // Objects
        public HashSet<Entity> Actors;
        public HashSet<Entity> Items;
        public HashSet<Entity> Props;
        public Entity Player;

        public void Create(int x, int y)
        {
            Bounds = new recti(0, 0, x, y);

            IsSolid = new Grid<bool>(x, y);
            IsSolidLight = new Grid<bool>(x, y);
            IsIndoor = new Grid<bool>(x, y);

            IsVisible = new Grid<bool>(x, y);
            IsRevealed = new Grid<bool>(x, y);

            Icon = new Grid<int>(x, y);
            Merge = new Grid<Direction>(x, y);

            Actors = new HashSet<Entity>();
            Items = new HashSet<Entity>();
            Props = new HashSet<Entity>();
            Player = null;
        }

        public void LoadMap()
        {
            Create(48, 32);

            Grid<int> tiles = ParseFile("res/data/tiles.txt", 48, 32);
            Grid<int> solid = ParseFile("res/data/solid.txt", 48, 32);
            Grid<int> props = ParseFile("res/data/props.txt", 48, 32);

            rectf[] frames = Frames.Create(0, 0, 16, 16, 256, 16);
            foreach (var POS in Bounds)
            {
                switch (solid[POS])
                {
                    case -1: Tile.SetTile(POS, false, false, false); break;
                    case 0: Tile.SetTile(POS, true, true, true); break;
                    case 1: Tile.SetTile(POS, false, false, true); break;
                }

                Tile.SetGloss(POS, tiles[POS]);

                if (props[POS] != -1)
                {
                    Entity p = Entity.CreateProp("Prop", POS);
                    p.Name = p.Name + "_" + p.ID;

                    p.Gloss.Frames.Source = new rectf[] { frames[props[POS]] };
                }
            }

            foreach (var POS in Bounds)
                Tile.SetMerge(POS);

            Feedback.AddMessage("Welcome to Mischief Manor!");
        }

        public void PopulateMap(recti bounds, int min, int max)
        {
            int attempts = Combat.Roll.Next(min, max + 1);

            for (int i = 0; i < attempts; i++)
            {
                v2i pos = (v2i)(Combat.Roll.Next2f() * (v2f)bounds.Size);
                if (!IsSolid[pos] && Entity.ActorAt(pos) == null)
                {
                    Entity e = new Entity();

                    if (Combat.Roll.NextFloat() <= 0.50f)
                        Entity.SetMouseVars(e, Drawing.Palette["yellow2"]);
                    else
                        Entity.SetBatVars(e, Drawing.Palette["blue1"]);
                    e.Pos = pos;

                    Actors.Add(e);
                }
            }
        }

        public Grid<int> ParseFile(string path, int w, int h)
        {
            Grid<int> result = new Grid<int>(w, h);

            string[] file = System.IO.File.ReadAllLines(path);

            for (int y = 0; y < file.Length; y++)
            {
                string[] line = file[y].Split(',');
                for (int x = 0; x < line.Length; x++)
                {
                    result[x, y] = Parse.AsInt32(line[x]);
                }
            }

            return result;
        }
    }
}