﻿/*
 * Kobold Cleaning!
 * 7DRL Entry - https://7drl.com/
 *
 * Author: Jackalsoft Games
 * Date:   03/05/2022 to 03/12/2022
 * 
 * This code is released under the GPL v3.0 license.
 * All other rights (c) their respective parties.
 */

namespace Kobold
{
    #region using

    using System;
    using System.Collections.Generic;

    using SFML;
    using SFML.Audio;
    using SFML.Graphics;
    using SFML.System;
    using SFML.Window;

    using Squish;
    using Squish.Collections;
    using Squish.Extensions;
    using Squish.Graphics;
    using Squish.Utilities;

    #endregion

    public class Camera
    {
        public Camera(int x, int y, int w, int h)
        {
            Position = new v2i(x, y);
            Size = new v2i(w, h);
        }

        public v2i Position;
        public v2i Size;

        public void MoveTo(v2i position)
        {
            Position = position - Size / 2;
        }

        public void MoveTo(int x, int y)
        {
            MoveTo(new v2i(x, y));
        }

        public recti GetBounds(recti clamp)
        {
            if (Position.X < clamp.X) Position.X = clamp.X;
            if (Position.Y < clamp.Y) Position.Y = clamp.Y;
            if (Position.X + Size.X > clamp.X + clamp.W) Position.X = clamp.X + clamp.W - Size.X;
            if (Position.Y + Size.Y > clamp.Y + clamp.H) Position.Y = clamp.Y + clamp.H - Size.Y;

            return new recti(Position, Size);
        }

        public recti GetBounds()
        {
            return new recti(Position, Size);
        }
    }
}