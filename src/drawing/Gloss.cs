﻿/*
 * Kobold Cleaning!
 * 7DRL Entry - https://7drl.com/
 *
 * Author: Jackalsoft Games
 * Date:   03/05/2022 to 03/12/2022
 * 
 * This code is released under the GPL v3.0 license.
 * All other rights (c) their respective parties.
 */

namespace Kobold
{
    #region using

    using System;
    using System.Collections.Generic;

    using SFML;
    using SFML.Audio;
    using SFML.Graphics;
    using SFML.System;
    using SFML.Window;

    using Squish;
    using Squish.Collections;
    using Squish.Extensions;
    using Squish.Graphics;
    using Squish.Utilities;

    #endregion

    public class Gloss
    {
        public Gloss()
        {
            Frames = new Frames();
            AnimState = 0;
            AnimSpeed = 2;

            DrawArgs = new SpriteArgs();
            FlipH = false;
            FlipV = false;
        }

        public Frames Frames;
        public float AnimState;
        public float AnimSpeed;

        public SpriteArgs DrawArgs;
        public bool FlipH;
        public bool FlipV;

        public void SetFrames(Texture texture, rectf[] source, float speed)
        {
            Frames.Texture = texture;
            Frames.Source = source;
            AnimState = 0.00f;
            AnimSpeed = speed;
        }

        public void Update(Time time)
        {
            float delta = time.AsSeconds();
            AnimState += AnimSpeed * delta;
        }

        public Quad GetQuad()
        {
            Texture texture = Frames.Texture;
            rectf source = Frames.GetFrame(AnimState);

            if (FlipH) source.W *= -1;
            if (FlipV) source.H *= -1;

            return Quad.FromSprite(DrawArgs.Position, DrawArgs.Size, texture, source, DrawArgs.Color, DrawArgs.Origin, DrawArgs.Rotation, DrawArgs.Depth);
        }
    }
}