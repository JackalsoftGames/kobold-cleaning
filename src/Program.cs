﻿/*
 * Kobold Cleaning!
 * 7DRL Entry - https://7drl.com/
 *
 * Author: Jackalsoft Games
 * Date:   03/05/2022 to 03/12/2022
 * 
 * This code is released under the GPL v3.0 license.
 * All other rights (c) their respective parties.
 */

namespace Kobold
{
    #region using

    using System;
    using System.Collections.Generic;

    using SFML;
    using SFML.Audio;
    using SFML.Graphics;
    using SFML.System;
    using SFML.Window;

    using Squish;
    using Squish.Collections;
    using Squish.Extensions;
    using Squish.Graphics;
    using Squish.Utilities;

    #endregion

    public static class Program
    {
        public static void Main()
        {
            var game = new GameInstance();
            {
                game.Window = new RenderWindow(new VideoMode(1024, 768), "Kobold Maid :: by Jackalsoft Games");
                game.Window.Closed += (sender, e) => { game.IsRunning = false; };
                game.Window.SetFramerateLimit(60);
                game.Window.SetIcon(16, 16, new Image("res/img/icon.png").Pixels);
                game.Run();
            }
        }
    }

    public sealed class GameInstance :
        Game
    {
        World w;

        DrawHelper drawing;
        Raycast2D raycast;
        HashSet<v2i> visible = new HashSet<v2i>();

        Random r = new Random(0);
        bool takingAction = false;

        public void SetPalette()
        {
            drawing.Palette["red1"] = c32.FromPacked(0xed4245);
            drawing.Palette["red2"] = c32.FromPacked(0xeb459e);

            drawing.Palette["yellow1"] = c32.FromPacked(0xfee75c);
            drawing.Palette["yellow2"] = c32.FromPacked(0xfaa81a);

            drawing.Palette["green1"] = c32.FromPacked(0x57f287);
            drawing.Palette["green2"] = c32.FromPacked(0x3ba55d);

            drawing.Palette["blue1"] = c32.FromPacked(0x5865f2);
            drawing.Palette["blue2"] = c32.FromPacked(0x2c3279); // custom (blue / 2)

            drawing.Palette["gray1"] = c32.FromPacked(0xacafb3); // custom (grey + 808080)
            drawing.Palette["gray2"] = c32.FromPacked(0x2c2f33); 

            drawing.Palette["white"] = c32.FromPacked(0xffffff);
            drawing.Palette["black"] = c32.FromPacked(0x000000);
        }

        public void SetInput()
        {
            Window.KeyPressed += Debug.HandleInput;
            Window.KeyPressed += (sender, e) =>
            {
                Direction d = 0;
                bool wait = false;

                switch (e.Code)
                {
                    case Keyboard.Key.W: d = Direction.N; break;
                    case Keyboard.Key.S: d = Direction.S; break;
                    case Keyboard.Key.A: d = Direction.W; break;
                    case Keyboard.Key.D: d = Direction.E; break;

                    case Keyboard.Key.Up: d = Direction.N; break;
                    case Keyboard.Key.Down: d = Direction.S; break;
                    case Keyboard.Key.Left: d = Direction.W; break;
                    case Keyboard.Key.Right: d = Direction.E; break;

                    case Keyboard.Key.Numpad8: d = Direction.N; break;
                    case Keyboard.Key.Numpad2: d = Direction.S; break;
                    case Keyboard.Key.Numpad4: d = Direction.W; break;
                    case Keyboard.Key.Numpad6: d = Direction.E; break;

                    case Keyboard.Key.Numpad7: d = Direction.NW; break;
                    case Keyboard.Key.Numpad9: d = Direction.NE; break;
                    case Keyboard.Key.Numpad1: d = Direction.SW; break;
                    case Keyboard.Key.Numpad3: d = Direction.SE; break;

                    case Keyboard.Key.Numpad5: wait = true; break;
                }

                if (takingAction)
                {
                    takingAction = false;
                    if (wait)
                    {
                        w.Feedback.AddMessage("Action cancelled.");
                        return;
                    }
                    else
                    {
                        v2i pos = w.Player.Pos.Step(d);
                        Entity p = w.Entity.PropAt(pos);

                        if (p == null)
                        {
                            w.Feedback.AddMessage("Action cancelled.");
                            return;
                        }

                        p.Vars["open"] = 0;
                        p.Vars["close"] = 1;
                        return;
                    }
                }

                if (e.Code == Keyboard.Key.C)
                {
                    takingAction = true;
                    w.Feedback.AddMessage("Close door (choose direction)");
                    return;
                }

                if (wait || d != 0)
                {
                    w.Entity.Step(w.Player, d);

                    List<Entity> updateList = new List<Entity>();
                    foreach (var ITEM in w.Actors)
                    {
                        if (ITEM == w.Player)
                            continue;

                        if (r.NextFloat() <= 0.50f)
                            updateList.Add(ITEM);
                    }

                    foreach (var ITEM in updateList)
                        w.Entity.Step(ITEM, r.GetNextDirection());
                }
            };

            Window.KeyPressed += (sender, e) =>
            {
                UpdateCamera(w.Player.Pos);
            };

            Window.KeyPressed += (sender, e) =>
            {
                v2i offset = v2i.Zero;

                switch (e.Code)
                {
                    case Keyboard.Key.Q:
                        w.PopulateMap(w.Bounds, 20, 50);
                        break;

                    case Keyboard.Key.E:
                        {
                            Entity a = w.Entity.CreateActor("", w.Player.Pos);

                            int n = r.Next(0, 2);

                            if (n == 0) w.Entity.SetMouseVars(a, drawing.Palette["yellow2"]);
                            if (n == 1) w.Entity.SetBatVars(a, drawing.Palette["blue1"]);
                        }
                        break;

                    case Keyboard.Key.R:
                        {
                            if (!w.Combat.Stats.ContainsKey(w.Player))
                                w.Combat.Stats[w.Player] = new CombatStats(w.Player);

                            w.Entity.DoPlayerStats();
                            w.Feedback.AddMessage("Rolling stats");
                        }
                        break;
                }
            };
        }



        

        public void UpdateCamera(v2i pos)
        {
            var bounds = w.Bounds;

            drawing.Camera.MoveTo(w.Player.Pos);
            drawing.Camera.GetBounds(bounds);

            visible.Clear();
            w.IsVisible.Fill(false);
            raycast.Apply(w.Player.Pos, drawing.Camera.Size.X / 2 + 1);
        }

        public override void Initialize(Time time)
        {
            w = new World(Window);
            drawing = w.Drawing;

            SetInput();
            SetPalette();

            w.LoadMap();
            w.Player = w.Entity.CreateActor("Kobold", new v2i(32, 26));
            w.Entity.DoPlayerStats();

            w.Player.Gloss.SetFrames(drawing.Texture, Frames.Create(0, 0, 16, 16, 4, 4), 2);
            w.Player.Gloss.DrawArgs.Color = drawing.Palette["green2"];

            w.Player.textAttack = "shoos";
            w.PopulateMap(w.Bounds, 5, 20);

            #region raycast

            raycast = new Raycast2D();
            raycast.Solid = (x, y) =>
            {
                if (!w.Tile.Exists(x, y))
                    return true;

                Entity p = w.Entity.PropAt(new v2i(x, y));
                if (p != null)
                {
                    return p.Vars.IsNullOrZero("open");
                }

                return w.IsSolidLight[x, y];
            };

            raycast.Visible = (x, y) =>
            {
                if (!w.Tile.Exists(x, y))
                    return;

                visible.Add(new v2i(x, y));
                w.Tile.SetVisible(new v2i(x, y), true, true);
            };

            #endregion

            UpdateCamera(w.Player.Pos);
        }

        public override void Update(Time time)
        {
            foreach (var ITEM in w.Actors)
            {
                ITEM.Gloss.Update(time);
                ITEM.Gloss.DrawArgs.Position = (v2f)ITEM.Pos;
                ITEM.Gloss.Frames.Texture = drawing.Texture;
            }

            foreach (var ITEM in w.Props)
            {
                ITEM.Gloss.Update(time);
                ITEM.Gloss.DrawArgs.Position = (v2f)ITEM.Pos;
                ITEM.Gloss.Frames.Texture = drawing.Texture;
            }
        }

        public override void Draw(Time time)
        {
            Window.Clear();
            {
                drawing.Buffer.Begin();
                {
                    drawing.DrawBox(24 - 1, 24 - 1, 720 + 2, 720 + 2, null, rectf.Empty, c32.White);
                    drawing.DrawBox(24, 24, 720, 720, null, rectf.Empty, c32.Black);
                }
                drawing.Buffer.End();
                Window.Draw(drawing.Buffer, RenderStates.Default);

                drawing.Buffer.Begin();
                {
                    recti bounds = drawing.Camera.GetBounds();
                    drawing.DrawTiles(w, bounds, 0);

                    foreach (var ITEM in w.Props)
                    {
                        if (!visible.Contains(ITEM.Pos))
                            continue;

                        drawing.DrawEntity(ITEM);
                    }

                    foreach (var ITEM in w.Actors)
                    {
                        if (!visible.Contains(ITEM.Pos))
                            continue;

                        float p = w.Combat.Stats[ITEM].Life / (float)w.Combat.Stats[ITEM].LifeMax;
                        drawing.DrawEntity(ITEM);
                    }
                }
                drawing.Buffer.End();

                RenderStates rs = RenderStates.Default;
                rs.Transform =
                    m33.CreateTranslation(24, 24) *
                    m33.CreateScale(48, 48) *
                    m33.CreateTranslation(-drawing.Camera.Position.X, -drawing.Camera.Position.Y);

                Window.Draw(drawing.Buffer, rs);
            }

            // Draw Text
            {
                int max = Math.Min(8, w.Feedback.Messages.Count);
                c32 c2 = c32.Transparent;


                drawing.Buffer.Begin();
                {
                    Quad q = Quad.FromSprite(24, 768 - 24, 768 / 4, -16 - max * 16, null, rectf.Empty, c32.Black * 0.50f, 0);
                    q.B.Color = c32.Transparent;
                    q.C.Color = c32.Transparent;

                    drawing.Buffer.Add(q);
                }
                drawing.Buffer.End();
                Window.Draw(drawing.Buffer, RenderStates.Default);

                for (int i = 0; i < max; i++)
                {
                    int n = w.Feedback.Messages.Count - i - 1;

                    c32 c = w.Feedback.Color[n].Lerp(c2, i / (float)max * 0.25f);
                    string str = w.Feedback.Messages[n];

                    drawing.DrawText(str, c, 32, 768 - 48 - i * 16, 16);
                }
            }

            // Debug text
            if (!Debug.Enabled)
            {
                string str = "";

                str += String.Format("cam: {0}\nkob: {1}\n",
                    drawing.Camera.GetBounds(), w.Player.Pos);
                str += "\n\n";

                str += String.Format("Debug\n");
                str += String.Format("\t Enabled {0}\n", Debug.Enabled);
                str += String.Format("\t Paused {0}\n", Debug.Paused);
                str += String.Format("\t Mode {0}\n", Debug.Mode);
                str += String.Format("\t Index {0}\n", Debug.Index);
                drawing.DrawText(str, c32.White, 768, 16, 16);

            }
            {
                string str = String.Format("Critters: {0}", w.Actors.Count);
                w.Drawing.DrawText(str, c32.White, 768 - 116, 768 - 24, 16);
            }

            DrawStatText(w.Combat.Stats[w.Player], 768, 256);
            Window.Display();
        }

        public void DrawStatText(CombatStats stats, float x, float y)
        {
            string str = "";

            str = "";
            str += String.Format("Str\nDex\nCon\nInt\nWis\nCha");
            drawing.DrawText(str, c32.White, x, y, 16);

            str = "";
            str += String.Format("{0} ({1})\n", stats.Str, w.Combat.GetMod(stats.Str));
            str += String.Format("{0} ({1})\n", stats.Dex, w.Combat.GetMod(stats.Dex));
            str += String.Format("{0} ({1})\n", stats.Con, w.Combat.GetMod(stats.Con));
            str += String.Format("{0} ({1})\n", stats.Int, w.Combat.GetMod(stats.Int));
            str += String.Format("{0} ({1})\n", stats.Wis, w.Combat.GetMod(stats.Wis));
            str += String.Format("{0} ({1})\n", stats.Cha, w.Combat.GetMod(stats.Cha));
            drawing.DrawText(str, c32.White, x + 32, y, 16);

            str = "";
            str += String.Format("Life\n");
            str += String.Format("To Hit\n");
            str += String.Format("Damage\n");
            str += String.Format("Dodge\n");
            str += String.Format("Armor\n");
            drawing.DrawText(str, c32.White, x + 96, y, 16);

            str = "";
            str += String.Format("{0}/{1}\n", stats.Life, stats.LifeMax);
            str += String.Format("{0}\n", stats.ToHit);
            str += String.Format("({0}~{1})\n", stats.DamageMin, stats.DamageMax);
            str += String.Format("{0}\n", stats.Dodge);
            str += String.Format("{0}\n", stats.Armor);
            drawing.DrawText(str, c32.White, x + 128+32, y, 16);
        }

        public static int DeadBats = 0;
        public static int DeadMice = 0;
    }
}