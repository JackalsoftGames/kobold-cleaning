﻿/*
 * Kobold Cleaning!
 * 7DRL Entry - https://7drl.com/
 *
 * Author: Jackalsoft Games
 * Date:   03/05/2022 to 03/12/2022
 * 
 * This code is released under the GPL v3.0 license.
 * All other rights (c) their respective parties.
 */

namespace Kobold
{
    #region using

    using System;
    using System.Collections.Generic;

    using SFML;
    using SFML.Audio;
    using SFML.Graphics;
    using SFML.System;
    using SFML.Window;

    using Squish;
    using Squish.Collections;
    using Squish.Extensions;
    using Squish.Graphics;
    using Squish.Utilities;

    #endregion

    public class Entity
    {
        public int ID;
        public string Name;
        public v2i Pos;

        public Gloss Gloss = new Gloss();
        public VarSet Vars = new VarSet();

        public string textAttack = "";
        public string textDead = "";

        public override string ToString()
        {
            return String.Format("[{0}] ID({1}) Name({2}) Pos({3})",
                GetType().Name, ID, Name, Pos);
        }
    }
}