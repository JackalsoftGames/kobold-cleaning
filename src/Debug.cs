﻿/*
 * Kobold Cleaning!
 * 7DRL Entry - https://7drl.com/
 *
 * Author: Jackalsoft Games
 * Date:   03/05/2022 to 03/12/2022
 * 
 * This code is released under the GPL v3.0 license.
 * All other rights (c) their respective parties.
 */

namespace Kobold
{
    #region using

    using System;
    using System.Collections.Generic;

    using SFML;
    using SFML.Audio;
    using SFML.Graphics;
    using SFML.System;
    using SFML.Window;

    using Squish;
    using Squish.Collections;
    using Squish.Extensions;
    using Squish.Graphics;
    using Squish.Utilities;

    #endregion

    public static class Debug
    {
        public static bool Enabled = true;
        public static bool Paused = false;

        public static int Mode = 0;
        public static int Index = 0;

        public static void HandleInput(object sender, KeyEventArgs e)
        {
            int n = 0;
            switch (e.Code)
            {
                case Keyboard.Key.F1: Mode = 0; break;
                case Keyboard.Key.F2: Mode = 1; break;
                case Keyboard.Key.F3: Mode = 2; break;
                case Keyboard.Key.F4: Mode = 3; break;
                case Keyboard.Key.F5: Mode = 4; break;
                case Keyboard.Key.F6: Mode = 5; break;
                case Keyboard.Key.F7: Mode = 6; break;
                case Keyboard.Key.F8: Mode = 7; break;

                case Keyboard.Key.Num1: n = 1; break;
                case Keyboard.Key.Num2: n = 2; break;
                case Keyboard.Key.Num3: n = 3; break;
                case Keyboard.Key.Num4: n = 4; break;
                case Keyboard.Key.Num5: n = 5; break;
                case Keyboard.Key.Num6: n = 6; break;
                case Keyboard.Key.Num7: n = 7; break;
                case Keyboard.Key.Num8: n = 8; break;
                case Keyboard.Key.Num9: n = 9; break;
                case Keyboard.Key.Num0: n = 10; break;

                case Keyboard.Key.Dash: Index--; break;
                case Keyboard.Key.Equal: Index++; break;

                case Keyboard.Key.Tilde: Enabled = !Enabled; break;
                case Keyboard.Key.Space: Paused = !Paused; break;
            }

            if (n != 0)
            {
                bool relative = e.Shift;
                bool negative = e.Control;

                if (negative) n *= -1;
                if (relative || negative)
                {
                    Index += n;
                }
                else
                {
                    if (n == 10)
                        n = 0; // Zero was pressed with no modifiers

                    Index = n;
                }
            }
        }

    }
}